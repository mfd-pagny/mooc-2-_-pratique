# Interview Tessa Lelièvre-Osswald 3/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* 1/5 [Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours](./1_interview_Tessa_Lelievre-Osswald1_5.md)  ? 
* 2/5 [Comment se former et préparer ses cours ?](./1_interview_Tessa_Lelievre-Osswald2_5.md) 
* **3/5 Pratique en classe\.**
* 4/5 [Enseignement adapté aux différents élèves\.](./1_interview_Tessa_Lelievre-Osswald4_5.md)
* 5/5 [Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)


## 3/5 Pratique en classe

Dans cette vidéo, Tessa Lelièvre-Osswald nous parle de sa première année d'enseignement, nous raconte comment elle a vécu cette première expérience, nous décrit une leçon type  ...

[![Interview Tessa Lelièvre-Osswald 3/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-3.mp4)


### Profiter de bonnes conditions et parcourir le programme en spirale.

_Alors, comment s'est passée cette première année d'enseignement que tu viens de vivre ?_

Alors, elle s'est je dirais très bien passé déjà, j'avais de très bonnes conditions puisque j'avais 2 groupes de terminale, un groupe de 12 élèves et un groupe de 17. Donc déjà, par rapport à d'autres collègues qui ont des classes de 35, c'est quand même un cadre de travail très agréable. Et également, j'avais forcément des appréhensions, puisque j'ai eu des anciens enseignants à moi qui me disaient : « mais tu sais, être prof, c'est très dur ... le contact avec les élèves, c'est compliqué ». Mais en fait, j'ai rapidement réussi à faire en sorte qu'on se sente bien avec les élèves, qu'eils aient confiance en moi. Et il n'y avait pas de problème de respect ou quoi que ce soit. Tout ça s'est vraiment très bien passé.

_Au niveau des aspects matériels. Qu'est ce que tu pourrais nous partager ?_

Alors moi, j'ai eu la chance d'avoir une salle informatique où systématiquement, j'ai un ordi par élève, ce qui est pratique, mais par contre, il ne faut pas oublier qu'ils ont pas forcément tous un ordi personnel à la maison. Donc, quand on créé le cours, les devoirs, moi, je faisais toujours en sorte que les devoirs soient plus sur des parties théoriques qu'ils peuvent faire sur papier, sans s'attendre à ce qu'il aient forcément un ordi à disposition. Et je leur conseillais dans le pire des cas, on va dire, quand c'était vraiment nécessaire de coder à la maison un site web qui s'appelle [replit](https://replit.com/languages/python3) dont ils peuvent se servir sur portable, ce qui n'est pas très pratique. Donc j'évitais.

_Alors pour nous rendre les choses plus concrètes. Et ça. Est ce que, par exemple, tu peux nous décrire une leçon type ?_

Oui, tout à fait. Donc, selon les notions, je vais commencer soit par l'exemple, soit par le cours. Le plus souvent, c'était important d'avoir d'abord du cours puisqu'il y a forcément du vocabulaire qui est utilisé, notamment. Donc, en fait, je répartis en général une définition, un exemple et un exercice, à chaque fois, pour que les élèves aient tout le temps quelque chose à faire, en quelque sorte. Ceci avec peut-être 5 ou 6 définitions, que ce soit celle d'un concept, d'un vocabulaire ou autre.

_Un exemple concret ?_

Ben oui, par exemple, le chapitre que je fais toujours en premier, c'est les graphes. Donc, j'explique ce qu'est un graphe en parlant des réseaux sociaux. Pour le graphe non orienté, on est sur Facebook, le graphe orienté en est sur Instagram et après je leur donne par exemple la manière dont on va écrire la matrice d'adjacence d'un graphe, donc sous la forme d'une petite définition. Et de là, eils ont un exemple. Et puis, en exercice, je leur dessine des graphes et ils doivent faire les matrices d'adjacence.

_Est ce que tu pourrais brièvement nous décrire ta progression sur l'année, par rapport au programme ?_

Moi, j'ai essayé de faire une progression, ce qu'on appelle spiralée, c'est à dire entre les différents thèmes, alterner un peu. J'ai commencé par les graphes puisque il n'y a pas vraiment besoin de prérequis. Donc, comme ça, même ceux qui n'ont pas beaucoup de souvenirs de l'année passée et qui n'ont pas forcément été présents à cause du covid ... au moins, tout le monde est dans le même bain : c'est un nouveau concept. Et après, au fur et à mesure, ça me permet quand je revenais par exemple à l'algorithmique des graphes sur laquelle je reviens plus tard dans ma spirale, je leur dire maintenant, vous révisez les graphes, on va faire la semaine prochaine l'algo des graphes. Et d'alterner un peu pour que même s'il y a des thèmes qui aiment moins, il n'en aient pas trop à la suite.

_Et qu'en est il des aspects d'évaluation en particulier, bien sûr, en lien avec la préparation au bac ?_

Alors moi, j'ai essayé de les évaluer sur pas mal de critères différents, que ce soit assez varié. Donc je leur ai fait faire des exposés, ce qui aide pour le grand oral, notamment savoir prendre la parole, être à l'aise. Je leur ai fait faire des contrôles sur papier, bien sûr, puisque comme au vrai bac, ce sera sur papier, avec des exercices un peu similaires. Pas trop de QCM, puisque ça, on en a qu'en première et pas en terminale. Et puis, bien sûr, sur ordinateur, des TP (travaux pratiques), puisqu'ils ont l'épreuve pratique, en toute logique. Et pour évaluer, j'ai toujours des grilles très prédéfinies pour être sûr de les évaluer justement. Et quand je dois évaluer du code, quand ça ne fonctionne pas, mon système, c'est je modifie leur code jusqu'à ce que ça fonctionne et je regarde combien de modifications j'ai dû faire pour savoir quel est le pourcentage des points eils vont obtenir.

_C'est très intéressant. C'est amusant parce que scientifiquement, ça ressemble à ce qu'on appelle les distances d'édition qui permettent de trouver quelle distance, en quelque sorte, il y a entre deux textes ._

