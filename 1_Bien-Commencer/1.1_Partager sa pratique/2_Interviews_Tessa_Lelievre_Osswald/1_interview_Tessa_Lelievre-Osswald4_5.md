# Interview Tessa Lelièvre-Osswald 4/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques) :
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* [1/5 Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours ?](./1_interview_Tessa_Lelievre-Osswald1_5.md) 
* 2/5 [Comment se former et préparer ses cours ?](./1_interview_Tessa_Lelievre-Osswald2_5.md) 
* 3/5 [Pratique en classe\.](./1_interview_Tessa_Lelievre-Osswald3_5.md)
* **4/5 Enseignement adapté aux différents élèves\.**
* 5/5 [Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)


## 4/5  Enseignement adapté aux différents élèves.

Dans cette vidéo, Tessa Lelièvre-Osswald répond aux différentes questions qui se posent à l'enseignant et enseignante de NSI : _qu’as-tu pu faire par rapport aux élèves en difficulté ? Pourquoi utiliser discord ? Et au niveau des aspects garçon fille ?_ ...


[![Interview Tessa Lelièvre-Osswald 4/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-4.mp4)


### S'adpater aux besoins des élèves au fil de l'année.

_Et qu'est ce que tu as pu faire par rapport aux élèves qui pouvaient avoir des difficultés ?_

Là dessus, ça dépend forcément du type de difficulté. Par exemple, j'avais un élève qui était dyslexique, donc moi, j'ai toujours fait en sorte, puisque j'ai des créneaux de deux heures, de faire mes contrôles en première heure pour lui donner un peu plus de temps. Et en deuxième heure, que ce soit ben du TP pour que la partie qu'il manque, puisqu'il continue à faire le contrôle, ne devienne pas une lacune, donc ne pas faire du cours sur la deuxième heure. Donc ça, c'est sur ce type de difficulté. Après, quand j'ai eu des élèves avec des difficultés personnelles, j'en ai discuté avec eux, tout simplement. Donc, il y en à qui j'ai donné plus de temps pour faire des devoirs ou à qui j'ai autorisé à faire qu'une partie des devoirs en fonction de leurs problèmes.

_Et des difficultés d'ordre plus scolaire ou plus didactique ? _

Oui, là dessus, j'ai eu aussi des moments où j'ai proposé à des élèves de venir parler un peu de méthode et également je j'avais ouvert un groupe [discord](https://discord.com/) puisque c'est une technologie avec laquelle eils sont très à l'aise, où je les autorisais toujours à me poser des questions. Donc, il y en a qui, parfois en cours, n'osent pas dire qu'ils ont du mal avec la méthode, mais qui vont finir par oser la poser sur discorde. Et ça m'est également arrivé de faire des petites vidéos que j'ai mises sur un compte YouTube pour leur réexpliquer des concepts avec lesquels ils avaient du mal ou des méthodes.

_Alors pourquoi discord ?_

Tout simplement, puisque je me suis rendu compte que tous mes élèves avaient discord, qu'eils l'utilisent  au quotidien et qu'ils l'ont sur le portable. Donc, par rapport à [pronote](https://www.index-education.com/fr/logiciel-gestion-vie-scolaire.php), où ils ne vont pas forcément se connecter très régulièrement, c'est beaucoup plus simple de les contacter. Et comme on a dû donner cours en distance, le fait qu'il y ait discord sur le portable, au moins c'était pratique parce qu'ils ne peuvent pas me faire croire qu'ils n'ont pas le micro sur le portable, par définition. _(par portable tu veux dire téléphone portable)_ Oui, tout à fait. 

_Et tu me dis aussi que c'est le seul outil numérique que tu as eu besoin d'utiliser ?_

Oui, voilà comme eils ont vraiment tous ça sur leur portable ou les 2 ou 3 qui n'avaient pas l'on installé sans souci, c'était suffisant. J'ai écrit, bien sûr, quand même des messages officiels sur pronote pour qu'il y ait des traces, mais c'était ce qu'on utilise le plus et c'était suffisant, en effet.

_Et au niveau des aspects garçons-filles ?_

Alors ça, c'est sûr que c'est un grand sujet puisque, comme on le sait, il y a beaucoup trop peu de femmes en informatique. Au moins, mes élèves, ils ont eu leurs deux enseignantes de NSI, donc, qui étaient des femmes. Donc déjà, ça leur montre que c'est possible. Après, j'avais quand même seulement 5 filles sur mes 29 élèves. Et ce que j'ai remarqué aussi, c'est qu'elles ont beaucoup de mal à avoir confiance en elles, par rapport aux garçons. Moi, notamment, en fin d'année, je leur ai fait remplir un petit questionnaire pour leur faire auto-évaluer la connaissance, la compréhension qu'eils avaient de ce qu'on a vu au cours de l'année. Donc, eils évaluent pour chaque notion si leur niveau était très élevé, élevé, moyen, plutôt bas, très bas. Et je me suis rendu compte que alors que j'avais des filles qui ont 19 en moyenne, elles se mettaient des moins bons niveau de compréhension que des garçons qui ont 12. On a encore du travail là dessus. Et j'ai envie de dire heureusement qu'elles m'avaient moi et que ça leur montrer qu'on peut très bien réussir en étant une fille dans l'informatique, qu'il n'y a pas de raison.

_Oui, t'as vraiment raison. C'est un grand sujet qu'on va [aborder](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/Le_Mooc/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/0_Introduction.html) à plusieurs niveaux dans ce MOOC._




