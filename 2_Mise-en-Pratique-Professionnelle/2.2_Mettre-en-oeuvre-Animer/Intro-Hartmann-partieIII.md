Dans sa troisième partie, _Enseigner l'Informatique_ nous adresse trois aspects sur lesquels il convient d'être vigilant au moment de créer la séance support de l'activité :

## Comment doser les différentes approches ? 

L'écart s'est creusé entre les principes théoriques et les applications pratiques, nous apprend on. Le risque est grand de ne pas intéresser les élèves dans une approche linéaire systématique de la théorie vers les applications pratiques. D'un autre côté, une approche purement pratique pourrait mettre en échec l'objectif de l'enseignement qui bien souvent est d'appréhender un concept fondamental pour aller vers une maîtrise de problèmes plus complexes. 

Cela ressemble à une évidence mais les auteurs concluent ce chapitre par l'idée qu'il faut varier les approches : descendante, ascendante, suivant l'évolution historique, débranchée, etc.

## Oublier les détails pour retenir l'essentiel

Une séance va être variée, plus ou moins longue, alterner des parties pratiques et d'autres plus théoriques. Comment faire pour ne pas perdre l'élève ? Comment réussir à ce que l'essentiel soit mis en avant (dans le but d'être mémorisé) et le détail laissé de côté ? C'est là probablement que l'animation de l'enseignant entre en jeu. Ménager des moments de restitution, s'assurer de la production d'une trace écrite sont des éléments auxquels il conviendra d'être attentif lors de la création de son activité.

## Planifier sa séance

Prévoir le contenu détaillé de sa séance relève de l'évidence. Mais il ne faut pas négliger de planifier _les à côtés_ : le matériel est-il en place ? les ordinateurs sont-ils opérationnels ? Ai-je bien préparé les éléments de mon activité débranchée ? Autant de questions qu'il faut se poser et auxquelles il convient de répondre en amont de la séance. Une perte de temps à cause d'un manque de planification peut engendrer beaucoup de frustration chez l'enseignant-e comme chez l'élève. L'objectif même de la séance risque d'être mis en échec.
