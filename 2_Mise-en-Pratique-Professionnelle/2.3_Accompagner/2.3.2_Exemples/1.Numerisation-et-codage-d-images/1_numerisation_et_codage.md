# Numérisation et codage d'images
# Du codage vers la numérisation : intentions 

## Du codage vers la numérisation : intentions 

La maîtrise de certains concepts informatiques peut prendre du temps : de plusieurs semaines à plusieurs mois pour les plus ardues.

l'écriture d'un nombre en base deux fait partie de celles-ci. Il n'est toutefois pas rare de voir dans des progressions, cette notion être travaillée en profondeur dès la rentrée de septembre en classe de première. En plus de poser de nombreuses difficultés mathématiques à certains élèves, elle peut donner l'impression qu'elle est fondamentale à maitriser en pré-requis de nombreux autres concepts mathématiques.

Nous proposons ici une activité permettant de formaliser très vite la notion d'écriture en un "mot binaire". Cela permet de parler très tôt de la notion d'écrire une information en un "mot binaire" et de le mettre en pratique très rapidement comme sur les images ici.

La liaison avec la notion d'écrire un nombre en base deux et passer du codage à la numérisation peut ainsi être échelonné et permettre à une appropriation plus lente qui nous semble convenir davantage à une majorité d'élèves de première.

À ce titre l'exercice 5 peut permettre, pour certains élèves, un première pas vers la numérisation au delà du simple codage.

Cette activité est construite à partir de l'une des excellentes idées du groupe ISO (Informatique Sans Ordi) : <a href="http://www.irem.univ-bpclermont.fr/Images-numeriques" target="_blank">lien vers le site</a>

La construction d'une image collaborative peut constituer un fil rouge de l'année et motiver ainsi des élèves à travailler la conversion décimale vers base deux tout au long de l'année selon les besoins ressentis.



## Exercice : Analyse de l'activité

Après avoir récupéré et lu : 
- L'activité élève proprement dite ;
- Un poster collaboratif ;
- Le poster collaboratif corrigé ;
- Le poste collaboratif "la totale" ;
- Une grille permettant de construire le poster collaboratif ;


nous vous proposons de **créer un fichier markdown**, dans votre espace de travail Git : _codage_image.md_ avec les entrées suivantes : 

    * Objectifs
    * pré-requis à cette activité
    * Durée de l'activité
    * Exercices cibles
    * Description du déroulement de l'activité
    * Anticipation des difficultés des élèves
    * Gestion de l'hétérogénéïté

et de la **compléter avec votre propre analyse**

puis nous en discuterons ensemble sur le <a href="https://mooc-forums.inria.fr/moocnsi/c/mooc-2/accompagner/194" target="_blank">forum</a>.

