# Projet Morpion

A partir du fichier [Morpion.pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/2_Mise-en-Pratique-Professionnelle/2.3_Accompagner/2.3.2_Exemples/Divers-projets/Morpion/Morpion.pdf?inline=false) vous devez programmer le jeu du Morpion.

Un fichier [Aide projets.pdf](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/2_Mise-en-Pratique-Professionnelle/2.3_Accompagner/2.3.2_Exemples/Divers-projets/Morpion/Aide%20projets.pdf?inline=false) peut être utilisé en cas de difficulté au début.
