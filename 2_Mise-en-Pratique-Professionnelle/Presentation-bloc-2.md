Nous voici au cœur de cette formation pour apprendre à enseigner… par la pratique, en co-préparant les activités pédagogiques des cours à venir, en partageant des pratiques didactiques et en prenant un recul pédagogique, y compris du point de vue de la pédagogie de l’égalité.

Cette mise en pratique professionnelle s'articule en quatre étapes d'une progression en quatre partie

- Penser, concevoir et élaborer une activité
- Mettre en oeuvre et animer cette activité
- Accompagner les élèves au cours de cette activité
- Observer, analyser et évaluer ce qui s'est passé

Chaque partie est articulé en 4 temps :

- Contextualiser : pour présenter les compétences, les objectifs visés et les actions à réaliser
- Apporter un soutien méthodologique : via l'ouvrage Enseigner l'informatique, Hartmann et al suivi d'un petit quiz pour se détendre
- S'inspirer / Créer / Discuter : des exemples de collègues pour vous inspirer, voir même les reprendre, ou au contraire s'en détacher pour créer vos propres ressources
- Échanger : proposer votre ressource à la communauté pour échanger sur votre travail, puis regarder et commenter ce que les autres ont proposé

Dans chacune des 4 parties, un travail personnel sera demandé.
Les 4 travaux réalisés seront à livrer à la fin du bloc 2 dans la séquence 2.5 Activité d'évaluation par les pairs.
