# Entretien avec Olivier Goletti 5/5 : de la didactique de l'informatique.

Olivier Goletti est un jeune chercheur en didactique de l'informatique. Ces entretiens permettent d'éclairer ce que recouvre 
ce nouveau champs disciplinaire.

## Entretien avec Olivier Goletti, Doctorant en didactique de l'informatique à l'UCLouvain

**Sommaire des 5 vidéos**

* 1/5 [Olivier Goletti, qui es-tu ?](./1_Entretien_Olivier_Goletti5_5.md)
* 2/5 [Didactique de l'informatique ?](./1_Entretien_Olivier_Goletti2_5.md ) 
* 3/5 [Quelques exemples\.](./1_Entretien_Olivier_Goletti3_5.md)
* 4/5 [Pensée informatique\.](./1_Entretien_Olivier_Goletti4_5.md)
* **5/5 Quelques conseils plus pratiques\.**


## 5/5 Quelques conseils plus pratiques\.

 Olivier Goletti, au fil des formations qu'il a données, a construit une liste de conseils en exemple de choses que l'on peut faire facilement de sa classe. Ses conseils sont inspirés de différentes recherches sur le sujet.
 
[![Entretien Olivier Goletti 5/5](https://mooc-nsi-snt.gitlab.io/portail/assets/Diapo-itw-OG1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-OG-5.mp4)

Quelques exemples de choses que l'on peut facilement faire dans sa classe, on ne reprendra pas les sources ici, ce sont des éléments utilisés dans les formations:

- _Donner des exemples de codes_ : des exemples différents pour ne pas mélanger le concept donné en exemple et le contexte de l'exemple; des exemples corrects à lire et des exemples à simplement recopier pour mémoriser; mais aussi des exemples avec des erreurs, pour s'entraîner à débugger, corriger, ceci en se raccrochant aux exemples corrects vus auparavant; et puis avec des propositions d'améliorations possibles à leur laisser faire pour réfléchir au delà de l'exemple.

- _Programmer en live_ : c'est aussi intéressant qu'on puisse programmer en direct devant la classe, en improvisation, cela permet de l'étudiant·e de s'identifier à un modèle, voir comment on fait, noter au passage un racourci clavier ou une autre astuce, assister à l'occurence d'une erreur qui sera détectée et corrigée (c'est autorisé de faire des erreurs, "même le prof en fait"), voir autrement et concrètement comment les choses se font, par exemple dans quel ordre; le sentiment d'appartenance à une communauté de pratique émerge de cette pratique, motivante et permettant de se sentir partie prenante. C'est aussi un moyen d'aider à montrer que c'est un objectif atteignable, donc ne pas faire de grande démonstrations "d'élite" mais des choses faisables.

- _Diversifier les types d'exerices_ : 
  - par exemple associer un ou des résultats à un ou plusieurs programmes, ce qui conduit à imaginer mentatement l'exécution du programme, et trouver des éléments qui valident ou invalident l'association; 
  - on peut aussi faire prédire l'exécution d'un programme; demander de faire un programme qui a le fonctionnement inverse (par exemple décale vers la droite au lieu de la gauche une liste) d'un programme vu en cours, c'est un exercice que l'on nomme "isomorphique" au sens où il réclame le même genre de compétences, mais avec un énoncé différent; 
  - demander des justifications après un choix, en expliquant à la fois pourquoi le choix retenu est correct, mais aussi pourquoi le choix écarté ne l'est pas; 
  - débugguer un programme à soi ou à une autre personne; 
  - utiliser des assistants, au sens d'aller expliquer à une autre personne comment s'en sortir après réussi un exercice, être capable d'expliquer le concept à quelqu'un d'autre est le stage suivant en matière de maîtrise d'un concept; 
  - combiner des activités connectées (sur machine) ou déconnectée (en utilisant une métaphore débranchée du concept ou de mécanisme à partir d'objets ou de situations du quotidien, ou papier crayon, ou de bricolage) pour retravailler des concepts sous d'autres formes, comme proposé, par exemple par [Marie Duflot-Kremer](https://members.loria.fr/MDuflot/files/med/index.html).

- _Penser l'organisation_ : 
  - apprendre à programmer en pair à pair, en faisant travailler deux étudiant·e·s ensembles, l'un·e tenant le clavier, l'autre observant le travail fait pour détecter des choses à corriger, compléter, offrir d'autres idées, avec un rôle de challenger, en inversant les rôles d'exercice en exercice;
  - mettre en évidence les étapes et objectifs du cours, expliquer où on en est dans le plan et où on va aller, à quoi cela va servir, en repositionnant par rapport aux cours précédents, on parle d'instruction explicite, qui permet de structurer les apprentissages de chaque personne;
  - les laisser s'expliquer entre elles et eux (notion d'instruction pair à pair) déjà abordé;
  - savoir dire "je ne sais pas [encore]", dans cette situation actuelle généralisée à tous les pays où on commence à enseigner l'informatique, et où les enseignant·e·s sont en train de finir de se former, il faut être à la fois transparent, et cela permet aussi de montrer comment on apprend, par exemple revenir la séance suivante avec la réponse et comment elle a été trouvée.






