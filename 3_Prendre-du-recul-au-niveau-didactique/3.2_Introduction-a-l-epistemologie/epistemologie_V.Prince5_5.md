# Epistémologie de l'informatique par Violaine Prince, 5/5- Les perspectives

Violaine Prince propose ici un cours, en 5 vidéos, sur l'epistémologie de l'informatique. Elle interroge les apports de l'informatique aux autres disciplines.

**Violaine Prince**

Violaine Prince est professeur à l'Université de Montpellier. Elle enseigne les matières suivantes :  algorithmique, modèles de calcul,  logique propositionnelle,  bases de données, cognition naturelle et artificielle, traitement du langage naturel, épistémologie de l’informatique...


**Sommaire des 5 vidéos**

* 1/5 [Définition de l'épistémologie\.](./epistemologie_V.Prince1_5.md)
* 2/5 [Le statut scientifique de l'informatique\.](./epistemologie_V.Prince2_5.md) 
* 3/5 [La fécondation croisée entre l'informatique et les autres sciences\.](./epistemologie_V.Prince3_5.md)
* 4/5 [Les grands domaines de l'informatique\.](./epistemologie_V.Prince4_5.md)
* **5/5 Les perspectives**


## 1/5 Définition de l'épistémologie  

[![Epistémologie de l'informatique 5/5](https://gitlab.com/mooc-nsi-snt/portail/-/raw/master/docs/assets/vignette-VPrince.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI2/05-VP.mp4)
