---
title: 17. Pédagogie de projet 
---

# Pédagogie de projet


Nous en sommes au troisième jour du stage de publication Web et le
formateur K., pendant la pause, déclare : « à chaque stage c'est la même
chose : les stagiaires ne veulent rien savoir d'une esquisse d'un site
Web sur papier et s'imaginent que je leur demande carrément de réaliser
un prototype de leur futur site sur une feuille. Le résultat est qu'au
lieu d'utiliser des feuilles de style CSS et de séparer ainsi le contenu
et la mise en forme, ils s'amusent à formater en code HTML. « C'est
pareil chez moi ! » dit sa collègue M. « Sous prétexte que nous
n'écrivons que de petits programmes pendant le cours, ils ne sont pas
documentés. Tout le monde se lance directement dans la programmation et
c'est à peine s'ils prennent connaissance des principes d'ingénierie
logicielle que j'essaie de leur inculquer. »

---

**Problème :** les systèmes informatiques sont souvent grands, complexes
et l'œuvre d'équipes de développeurs. Cependant, dans l'enseignement, le
temps manque pour planifier et construire ce genre de grand système et
la limitation à des équipements de taille réduite ne permet pas
d'appréhender certains aspects importants de l'informatique.


L'informatique fournit des stratégies et des méthodes pour résoudre des
problèmes complexes et construire de grands systèmes. Toutefois, il
n'est généralement pas possible, par manque de temps, d'étudier en
détail les grands systèmes dans le cadre d'une formation. L'ingénierie
logicielle, la gestion de projet ou encore la modélisation sont donc
traitées en théorie. Les exemples cités au cours de la formation
n'appliquent ces stratégies qu'à des situations simples. Les étudiants
ne prennent ainsi pas vraiment conscience de la manipulation des
systèmes complexes et leur impression en est faussée. Exemple :
l'orientation objet comme méthode de projet pour les applications
logicielles complexes ne peut être thématisée de manière appropriée aux
grands systèmes dans un cours qui s'adresse aux débutants. La tentation
est donc forte de recourir à des exemples simples tels que la
programmation orientée objet d'un feu de circulation. De tels exemples
ne peuvent cependant pas démontrer les avantages de la conception
orientée objet.


Comment, dans l'enseignement de l'informatique, en dépit d'un temps
limité, communiquer raisonnablement un aperçu des problèmes rencontrés
dans des projets informatiques réels, depuis la définition des
interfaces jusqu'aux différends entre les personnes au sein d'une équipe
de projet ? La méthode du projet est prédestinée à cet effet : comme
dans le monde réel, les participants planifient eux-mêmes la manière
dont ils veulent s'attaquer à un problème. Ils suggèrent différentes
propositions, définissent l'approche et établissent le calendrier. Le
plan est ensuite mis en œuvre dans le cadre d'un travail en équipe. Avec
la méthode du projet, les participants apprennent à définir des
objectifs qui peuvent être atteints dans le temps imparti. Des échéances
sont fixées, les tâches à accomplir partagées, les solutions partielles
évaluées puis rassemblées en un tout. La méthode du projet est une
méthode d'enseignement qui reflète de nombreux aspects d'un projet
informatique.


Il existe cependant également des différences entre un projet informatique 
et la méthode du projet en tant que méthode d’enseignement : 
dans les projets informatiques, les chefs de projet sont désignés à l’avance 
et les objectifs généralement fixés par le client. Avec la méthode du 
projet dans l’enseignement, il s’agit pour les participants d’endosser eux-mêmes
le rôle du chef de projet ; les objectifs sont donc définis collégialement 
par tous les participants. L’objectif d’un projet pédagogique n’est pas un produit
commercialisable, mais la promotion de l’indépendance, du travail en équipe et de
l’esprit critique. Etant donné qu’il existe une littérature abondante et de
qualité sur la méthode du projet (par exemple [Fre05]), elle ne sera pas décrite
en détail ici. Un exemple qui a fait ses preuves est le modèle à sept niveaux de
Karl Frey que nous allons ici brièvement résumer :

**Initiative du projet**  
C'est ici que sont soulevés les problèmes, les
tâches ou les idées. Les questions peuvent venir des enseignants, des
étudiants ou d'une autre source. L'initiative du projet doit être
ouverte, elle ne doit pas simplement consister à résoudre un problème
clairement défini.


**Délimitation**  
Il s'agit ici de trier les questions et les idées
soulevées. Les premières idées d'un projet sont souvent trop vagues et
ont besoin d'être précisées. En règle générale, en raison des ressources
limitées (temps, matériel, connaissances), il n'est pas non plus
possible d'explorer toute l'étendue de l'idée originale. Il faut
délimiter les objectifs du projet et fixer des priorités.


**Plan de projet**  
Après avoir fixé les objectifs du projet, il faut
établir un plan de travail. Les objectifs et la procédure à suivre sont
retenus, les activités à réaliser sont affectées à des groupes
individuels et la circulation des informations entre les groupes est
définie.


**Exécution**  
Dans cette phase, les activités prévues sont réalisées,
les résultats sont évalués continuellement puis comparés avec le plan de
projet. Une révision de ce dernier sera éventuellement nécessaire dans
cette phase, suivant l'évolution des travaux.


**Clôture**  
Dans l'enseignement de l'informatique, un projet se termine
souvent par la réalisation d'un système informatique de petite taille.
Il ne s'agit généralement pas d'un système réellement productif, mais
plutôt d'un prototype. Une présentation des résultats peut être une
bonne clôture d'un projet pédagogique.


**Échéances fixes**  
Il est très important de fixer des échéances
(étapes), plus particulièrement dans des projets à long terme ou des
projets où interviennent de nombreux participants. Les échéances fixes
garantissent que l'échange d'informations entre les participants a bien
lieu.


**Méta-interaction**  
L'objectif de la méthode du projet n'est pas
seulement un produit fonctionnel. Le projet doit également promouvoir
les compétences sociales et de gestion de projet des participants. Il
est par conséquent essentiel que ces derniers échangent continuellement
des informations sur l'évolution du projet : sommes-nous sur la bonne
voie ? Y a-t-il des points faibles ? La communication humaine
fonctionne-t-elle au sein du groupe de projet ? C'est autour de cette
« méta-interaction », à savoir la réflexion sur ses propres pensées et
actions, que s'articule toute la méthode du projet.


**Solution :** le temps manque dans l'enseignement pour planifier et
construire de grands systèmes informatiques. La méthode du projet permet
ici de présenter tout de même les phases et les aspects les plus
importants d'un projet informatique type.


Il est impossible de regrouper dans un livre de bons exemples de projets
pédagogiques ; les projets réels doivent être ouverts, d'actualité et
présenter un intérêt immédiat pour les participants. Dans l'idéal, les
projets éveillent la curiosité des stagiaires ou des étudiants et
développent une dynamique propre. Les exemples suivants décrivent
brièvement quelques projets pédagogiques afin de donner une idée des
multiples possibilités offertes par un projet.


**Exemple 1 : projet du robinet qui goutte**


Ce projet pédagogique a eu lieu dans un lycée et a été mené par Kurt
Doppler et l'un des auteurs. L'initiative est née de l'hospitalisation
d'un élève qui a observé le comportement d'une perfusion au
goutte-à-goutte et en a déduit des modèles intéressants. De retour à
l'école, il a proposé ce thème comme sujet de cours. L'élève, en
collaboration avec deux de ses camarades ainsi que deux enseignants, a
décidé d'aller au fond des choses. Pour inciter davantage d'élèves à
participer au projet, celui-ci a été épinglé sur le tableau d'affichage
de l'école.


Le comportement complexe d'un robinet qui goutte est tout à fait
surprenant au vu de la simplicité des forces en présence : un robinet
goutte normalement à un rythme régulier, mais il suffit de l'ouvrir un
tout petit peu plus pour que les gouttes tombent à des intervalles
irréguliers. L'ordre apparent cède la place à un chaos turbulent.


La capacité à prédire le comportement d'écoulement des fluides est d'une
grande importance dans de nombreuses applications. Le robinet qui goutte
démontre les difficultés rencontrées à essayer de mettre de l'ordre dans
le désordre.


Parviendrons-nous à construire une expérience de robinet qui goutte ?
L'égouttement peut-il être mesuré avec une précision suffisante ? Les
données mesurées peuvent-elles être acquises avec un ordinateur ? Que
révélera l'analyse des données ? Le phénomène du robinet qui goutte
peut-il être décrit par un modèle mathématique ?


Un plan de projet a été élaboré sur la base de l'initiative du projet de
l'élève et le travail a été divisé en différents groupes : un groupe
avait confectionné un goutte-à-goutte mécanique qui a permis un comptage
précis des gouttes en mesurant le niveau d'eau, un deuxième groupe s'est
occupé de la construction d'un compte-gouttes électronique pour mesurer
la séquence des gouttes et un troisième groupe a écrit le programme en
assembleur pour l'acquisition des données mesurées. Un autre groupe
avait pour tâche d'interpréter et de visualiser les données mesurées et
acquises. Cette configuration en groupes est typique de la méthode du
projet. Le travail en équipe est très important pour un projet, les
cavaliers seuls ne sont pas les bienvenus.


Des problèmes inattendus sont survenus au cours du projet : de petites
vibrations, par exemple provoquées par le passage d'un camion dans la
rue voisine, ont ainsi fortement perturbé les résultats de la mesure. Il
a fallu déplacer l'environnement de l'expérience dans une autre pièce et
mener les expériences pendant les heures calmes de la nuit.


Seuls des rythmes d'une, deux et quatre gouttes ont été constatés au
début. La théorie du chaos (figuier) laissait néanmoins également
entrevoir des cycles de trois, toutefois restés absents. Une nouvelle
révision du plan du projet a conduit à des mesures ininterrompues
pendant une nuit entière dans l'espoir de découvrir ainsi un cycle de
trois. Cette longue série de mesures a imposé une modification du
goutte-à-goutte et de l'acquisition des données mesurées ainsi qu'un
logiciel de reconnaissance de modèle qui recherchait spécifiquement les
cycles de trois. Il a été finalement possible de mesurer des cycles de
trois reproductibles et d'en dériver des cycles de six.


Une conférence de presse et un article dans le journal local ont marqué
la clôture du projet. De nombreuses méta-interactions ont eu lieu au
sein du groupe pendant toute la durée du projet. Des problèmes qui
semblaient au premier abord insolubles ont été décortiqués au cours de
séances de remue-méninges d'où sont sorties des idées parfois un peu
folles : puisque la mesure du temps menaçait d'échouer en raison de la
lenteur des logiciel son a décidé de porter le problème à la
connaissance des personnes connues. Le résultat a été l'adoption d'une
solution physique de mesure du temps à la place de la mesure assistée
par logiciel, ce qui a été un facteur important pour la réussite du
projet.


**Exemple 2 : projet LegoKara**


Ce projet a eu lieu dans le cadre d'un cours renforcé dans un lycée. Les
étudiants qui y ont participé ont fixé leurs propres objectifs au sein
de groupes. Le principal objectif de deux étudiants était d'approfondir
leurs connaissances de Java mais aussi d'apprendre à connaître et à
utiliser la conception orientée objet. Ils ont en outre recherché un
projet qui avait un contexte d'application réel. Ils se sont alors
tournés vers les développeurs du logiciel éducatif Kara \[RNH04\]. Kara
est une coccinelle virtuelle qui vit dans un monde simple. Elle peut
être programmée et ainsi accomplir différentes tâches, par exemple
ramasser des feuilles de trèfle. Les programmes de Kara sont des
séquences automatiques finies et sont créés dans un environnement de
développement graphique.


Le propriétaire du projet, les développeurs de Kara, ont collaboré avec
les deux étudiants pour élaborer l'objectif du projet LegoKara :
LegoKara devait devenir un robot de type Lego Mindstorms inspiré de Kara
qui, tout comme Kara, peut être programmé avec des séquences
automatiques finies. Un utilisateur de LegoKara devait pouvoir tester
ses programmes virtuellement avec Kara puis les télécharger sur le
robot.

Un objectif globalement ambitieux qui a fait apparaître clairement dès
le départ que les étudiants devront y investir bien plus que le temps
alloué par l'école.


**Le plan du projet avait en gros prévu les étapes suivantes :**


1. Incorporation dans l'architecture de Kara et recherche des
possibilités d'extension du système existant ainsi que les interfaces
nécessaires à cet effet.


2. Bricolage d'un prototype robuste de robot LegoKara possédant les
mêmes aptitudes que le Kara virtuel. Le coût du robot devait être
réduit, à savoir que toutes les pièces nécessaires devaient si possible
se trouver dans le kit Mindstorms standard.


3. Décider comment l'automate fini de Kara pourrait être compilé le
plus simplement possible en code octal Mindstorms.


4. Mise en œuvre proprement dite de LegoKara : le compilateur ainsi que
les adaptations nécessaires à l'interface d'utilisateur graphique et
l'intégration dans Kara.


5. Réception par le propriétaire du projet.


6. Publication sur le site Web de Kara.


Le plus grand défi a été la partie matérielle du projet : la
construction du robot a pris beaucoup plus de temps que prévu. Diverses
expériences ont été nécessaires afin que le robot soit assez stable et
ne se désagrège pas en cas de collision. La mise en œuvre du compilateur
-- le noyau du développement logiciel -- s'est déroulée sans aucune
difficulté. L'intégration dans un système orienté objet existant a
constitué une tâche inhabituelle pour les deux élèves, mais a été
parfaitement maîtrisée. Le fait qu'il ne s'agissait pas d'un projet
purement scolaire, mais d'une « incursion » dans le monde réel a été une
expérience très motivante pour les deux étudiants.


**Exemple 3 : travail de projet dans la sécurité d'information
appliquée**


Un cours de formation incluant un projet pour lequel le chargé de cours
avant fixé un objectif relativement serré a été dispensé plusieurs fois
à l'EPF de Zurich : dans le cadre du Laboratoire de sécurité appliquée,
les étudiants avaient la possibilité d'étudier en détail les aspects
applicatifs de la sécurité de l'information et d'utiliser dans un
contexte pratique les connaissances conceptuelles théoriques acquises
lors des cours \[NB05\].


La première partie du cours de formation avait pour thème principal la
communication du savoir : un matériel d'auto-formation avait été fourni
à cet effet. Ce matériel était complété par de nombreuses expériences
que les étudiants ont réalisées directement sur l'ordinateur. Ils ont
ainsi acquis le bagage nécessaire pour le projet à venir.


Les étudiants ont été divisés en groupes de quatre et ont reçu en guise
de tâche à réaliser pour le projet un bref cahier des charges pour une
application. Leur mission consistait à réaliser cette application. Ils
pouvaient choisir les moyens librement. Ils pouvaient également utiliser
des composants existants (généralement en Open Source) et les combiner
de manière appropriée ou mettre en œuvre eux-mêmes la plus grande partie
de l'application. Une partie du projet a donc été la conception du
système global, y compris l'architecture, la modélisation des données,
le concept de rôle, etc.


Les aspects de la sécurité ont en outre été un élément clé du projet.
Les étudiants devaient élaborer une analyse du risque de leur système,
puis en tirer des mesures de sécurité appropriées et les mettre en
œuvre. Après la phase de réalisation du projet, les groupes ont échangé
leurs systèmes et ont procédé à une revue de l'autre système respectif.
Ils devaient ici évaluer la conception du système tiers et la comparer
avec leur propre conception, vérifier la transposition correcte des
mesures de sécurité et découvrir les failles de sécurité restantes.


La sécurité d'information appliquée est un véritable défi : il est très
difficile en pratique de réaliser correctement un système fonctionnel
sans passer à côté de certains problèmes de sécurité. Ces difficultés ne
peuvent être démontrées que sur des systèmes ayant une certaine
complexité, ce qui impose d'y consacrer un certain temps. Le travail de
projet représente ici une approche possible. En outre, le projet oblige
les étudiants à composer avec des objectifs contradictoires tels que le
coût, le délai imposé, les exigences de sécurité ou la facilité
d'utilisation.

