# Fiche thématique

## Langages et Programmation

Thème présent à la fois dans le programme de Première et celui de Terminale. Des activités communes pourraient être proposées pour les deux niveaux.

### [Programme officiel (Première)](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
|Constructions élémentaires | Mettre en évidence un corpus de constructions élémentaires. | Séquences, affectation, conditionnelles, boucles bornées, boucles non bornées, appels de fonction. |
| Diversité et unité des langages de programmation | Repérer, dans un nouveau langage de programmation, les traits communs et les traits particuliers à ce langage. |  Les manières dont un même programme simple s’écrit dans différents langages sont comparées. |
| Spécification | Prototyper une fonction. <br>Décrire les préconditions sur les arguments. <br>Décrire des postconditions sur les résultats. | Des assertions peuvent être utilisées pour garantir des préconditions ou des postconditions. |
| Mise au point de programmes | Utiliser des jeux de tests. | L’importance de la qualité et du nombre des tests est mise en évidence. <br>Le succès d’un jeu de tests ne garantit pas la correction d’un programme. |
| Utilisation de bibliothèques | Utiliser la documentation d’une bibliothèque. | Aucune connaissance exhaustive d’une bibliothèque particulière n’est exigible.

### [programme officiel (Terminale)](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf) 

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Notion de programme en tant que donnée. <br>Calculabilité, décidabilité. | Comprendre que tout programme est aussi une donnée. <br>Comprendre que la calculabilité ne dépend pas du langage de programmation utilisé.<br>Montrer, sans formalisme théorique, que le problème de l’arrêt est indécidable. | L’utilisation d’un interpréteur ou d’un compilateur, le téléchargement de logiciel, le fonctionnement des systèmes d’exploitation <br>permettent de comprendre un programme comme donnée d’un autre programme. |
| Récursivité. | Écrire un programme récursif. <br>Analyser le fonctionnement d’un programme récursif. |  Des exemples relevant de domaines variés sont à privilégier. |
| Modularité. | Utiliser des API (Application Programming Interface) ou des bibliothèques. Exploiter leur documentation.<br>Créer des modules simples et les documenter. |
| Paradigmes de programmation. | Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet. <br>Choisir le paradigme de programmation selon le champ d’application d’un programme. | Avec un même langage de programmation, on peut utiliser des paradigmes différents.<br> Dans un même programme, on peut utiliser des paradigmes différents.