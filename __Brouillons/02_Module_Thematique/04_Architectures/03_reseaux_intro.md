# Fiche thématique

## Les réseaux

### [Programme officiel (Première)](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
|  Transmission de données dans un réseau<br> Protocoles de communication<br>Architecture d’un réseau | Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation.<br>Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné).<br>Simuler ou mettre en œuvre un réseau. | Le protocole peut être expliqué et simulé en mode débranché.<br>Le lien est fait avec ce qui a été vu en classe de seconde sur le protocole TCP/IP.<br>Le rôle des différents constituants du réseau local de l’établissement est présenté.



### [programme officiel (Terminale)](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf) 

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
| Protocoles de routage. | Identifier, suivant le protocole de routage utilisé, la route empruntée par un paquet. | En mode débranché, les tables de routage étant données, on se réfère au nombre de sauts (protocole RIP) ou au coût des routes (protocole OSPF).<br> Le lien avec les algorithmes de recherche de chemin sur un graphe est mis en évidence |
| Sécurisation des communications. | Décrire les principes de chiffrement symétrique (clef partagée) et asymétrique (avec clef privée/clef publique).<br> Décrire l’échange d’une clef symétrique en utilisant un protocole asymétrique pour sécuriser une communication HTTPS. | Les protocoles symétriques et asymétriques peuvent être illustrés en mode débranché, éventuellement avec description d’un chiffrement particulier.<br> La négociation de la méthode chiffrement du protocole SSL (Secure Sockets Layer) n’est pas abordée.