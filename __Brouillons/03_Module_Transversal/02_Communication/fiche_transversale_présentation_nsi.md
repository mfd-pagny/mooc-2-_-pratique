# Fiche transversale

## Présentation de NSI

Nous sommes au mois de mars, les conseils de classe du 2e trimestre approchent. Les élèves de seconde doivent fournir un préchoix des 3 spécialités qu’ils aimeraient suivre en première. Afin de les aider dans ce choix, votre chef d’établissement décide d’organiser une réunion d’information sur les spécialités avec les parents d'élèves et leurs enfants.
Pour cette réunion, le proviseur vous demande de préparer une présentation de la spécialité NSI. Votre lycée proposant 9 spécialités, le proviseur octroie uniquement 5 minutes par spécialité (+5 minutes de questions/réponses avec les parents).
Quelle forme prendra votre présentation ? Rédiger le support que vous utiliserez au cours de cette présentation. 

