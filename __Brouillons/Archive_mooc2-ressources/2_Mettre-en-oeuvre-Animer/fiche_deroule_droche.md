## Exemple de mise en oeuvre de séance

**Fiche élève cours :** https://pixees.fr/informatiquelycee/sec/s1_1.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/sec/s1_4.html

**Fiche prof :** [disponible ici](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_droche.md)

**Etape 0 :** Mise en place: 

- installation des élèves sur les machines, seul ou par deux, 
- connection des élèves à leur espace de travail, 
- identification du logiciel à lancer, lancement et choix de la langue,
- ouverture de la page web de la fiche élève cours et activité, 
- temps de lecture et visualisation de la vidéo d'introduction.

Après cette prise en main, il y aura un temps de questions.

On s'assurera en particulier que tous les termes "techniques" font sens.

On pourra laisser les élèves aller sur la page Web [Filius](https://www.lernsoftware-filius.de/Herunterladen) et voir comment utiliser https://translate.google.com pour la lire en anglais ou français.

**Etape 1 :** À faire vous-même 1

On observe là où les élèves bloquent:

- est-ce au niveau de l'ergonomie de l'interface ?
- les termes techniques font-ils du sens ? (ex: proposer de dire ce que tel terme signifie)
- une ou un élève peut-il décrire ce qu'il observe sur l'écran ?
- deux élèves peuvent-illes s'entraider pour réussir l'activité


**Etape 2 :** À faire vous-même 2

Prendre quelques captures d'écran pour expliquer ce qui a été fait sous forme de tutoriel visuels avec quelques lignes de légende.

**Etape 3 :** À faire vous-même 3

Prendre un peu de recul : noter combien de temps il a fallu pour réussir et quelques sont les étapes qui ont nécessité le plus de temps.
